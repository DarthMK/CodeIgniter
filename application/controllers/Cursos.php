<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('miprimermodelo_model');
	}

	function index(){
		$data['segmento'] = $this->uri->segment(3);
		$this->load->view('cursos/headers');

		if(!$data['segmento']){
			$data['cursos'] = $this->miprimermodelo_model->obtenerCursos();
		}
		else{
			$data['cursos'] = $this->miprimermodelo_model->mostrarCursoPorId($data['segmento']);
		}

		$this->load->view('cursos/cursos', $data);
	}

	function nuevo(){
		$this->load->view('cursos/headers');
		$this->load->view('cursos/formulario');
	}

	function recibirDatos(){
		$data = array(
			'nombre' => $this->input->post('nombre'),
			'videos' => $this->input->post('videos') );

		$this->miprimermodelo_model->crearCurso($data);

		$this->load->library('menu', array('Inicio', 'Contacto', 'Cursos'));
		$data['mi_menu'] = $this->menu->construirMenu();

		$this->load->view('cursos/headers');
		$this->load->view('miprimercontrolador/bienvenido', $data);
	}

	function editar(){
		$data['id'] = $this->uri->segment(3);
		$data['curso'] = $this->miprimermodelo_model->mostrarCursoPorId($data['id']);
		$this->load->view('cursos/headers');
		$this->load->view('cursos/editar', $data);
	}

	function actualizar(){
		$data = array(
			'nombre' => $this->input->post('nombre'),
			'videos' => $this->input->post('videos') );
		$this->miprimermodelo_model->actualizarCurso($this->uri->segment(3), $data);
		redirect(base_url());
	}

	function borrar(){
		$id = $this->uri->segment(3);
		$this->miprimermodelo_model->eliminarCurso($id);
		
	}

}
?>