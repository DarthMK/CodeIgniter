<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Miprimercontrolador extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('miprimermodelo_model');
	}
	function index(){
		$this->load->library('menu', array('Inicio', 'Contacto', 'Cursos'));
		$data['mi_menu'] = $this->menu->construirMenu();
		$this->load->view('miprimercontrolador/headers');
		$this->load->view('miprimercontrolador/bienvenido', $data);
	}
	function holaMundo(){
		$this->load->library('menu', array('Inicio', 'Contacto', 'Cursos'));
		$data['mi_menu'] = $this->menu->construirMenu();
		$this->load->view('miprimercontrolador/headers');
		$this->load->view('miprimercontrolador/bienvenido', $data);
	}

	
}
?>