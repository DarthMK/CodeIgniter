<body>
	<h1>Formulario</h1>
	<?= form_open("/cursos/recibirDatos"); ?>
	<?php
		$nombre = array(
			'name' => 'nombre' ,
			'placeholder' => 'Escribe el nombre del curso' );
		$videos = array(
			'name' => 'videos' ,
			'placeholder' => 'Cantidad de videos del curso' );
	?>
	<?= form_label('Nombre:', 'nombre'); ?>
	<?= form_input($nombre); ?>
	
	<br>
	
	<?= form_label('Número videos:', 'videos'); ?>
	<?= form_input($videos); ?>
	
	<br>
	
	<?= form_submit('', 'Subir curso'); ?>
	<?= form_close(); ?>
</body>
</html>