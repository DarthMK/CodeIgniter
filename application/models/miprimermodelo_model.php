<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Miprimermodelo_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function crearCurso($data){
		$this->db->insert('cursos', array(
			'nombreCurso' => $data['nombre'],
			'videosCurso' => $data['videos'] ) );
	}

	function obtenerCursos(){
		$query = $this->db->get('cursos');
		if($query->num_rows() > 0 ) return $query;
		else return false;
	}

	function mostrarCursoPorId($id){
		$this->db->where('idCursos', $id);
		$query = $this->db->get('cursos');
		if($query->num_rows() > 0 ) return $query;
		else return false;
	}

	function actualizarCurso($id, $datos){
		$data = array(
			'nombreCurso' => $datos['nombre'],
			'videosCurso' => $datos['videos'] );
		$this->db->where('idCursos', $id);
		$query = $this->db->update('cursos', $data);
	}

	function eliminarCurso($id){
		$this->db->delete('cursos', array('idCursos' => $id));
	}
}
?>